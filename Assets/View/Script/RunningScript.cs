﻿using UnityEngine;
using System.Collections;

public class RunningScript : MonoBehaviour {

	private bool falling= false;
	private int jumpHeight =1;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("speed", 7);
		falling = true;
	}
		
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * PlayerPrefs.GetInt ("speed") * Time.deltaTime);
		if (Input.GetKey (KeyCode.Space)) {
			if ((GetComponent<Rigidbody2D> ().position.y < jumpHeight) && falling == false) {
				//Debug.Log (GetComponent<Rigidbody2D> ().position.y);
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, 12);

			}

			else {
				falling = true;
			}

		}
		else {
			falling = true;
		}
	
	}

	void OnCollisionStay2D(){
		Debug.Log ("collided");
		falling = false;
	}


}