﻿using UnityEngine;
using System.Collections;

public class obstacle : MonoBehaviour {

	public GameObject[] ob;
	public Transform campos;
	public float gap = 1.5f;
	public int obs;

	// Use this for initialization
	void Start () {
		ObstacleMaker ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * PlayerPrefs.GetInt ("speed") * Time.deltaTime);
		GameObject go = GameObject.Find ("Quad1");
		if (campos.position.x - go.transform.position.x > 25) {
			Destroy (go);
		}
	}

	void ObstacleMaker(){
		GameObject clone;
		obs = Random.Range (0, ob.Length);
		if (obs == 6) {
			clone = (GameObject)Instantiate (ob [obs],new Vector3(transform.position.x+gap,-1,transform.position.z),Quaternion.identity);
		} else {
			clone = (GameObject)Instantiate(ob[Random.Range(0,ob.Length-1)],new Vector3(transform.position.x+gap,-2.8f,transform.position.z),Quaternion.identity);
		}
		clone.name = "Quad1";
		clone.AddComponent<BoxCollider2D> ();
		clone.GetComponent<BoxCollider2D> ().isTrigger = true;
		float xx = Random.Range (1,6);
		Invoke ("ObstacleMaker", xx);
	}
}
